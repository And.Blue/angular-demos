import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule} from "@angular/common/http";

import {AppComponent} from './app.component';
import {TopBarComponent} from './top-bar/top-bar.component';
import {MenuComponent} from './parent-child/menu/menu.component';
import {BehaviorSubjectHomeComponent} from './behavior-subject/behavior-subject-home.component';
import {WordBoxComponent} from "./behavior-subject/word-box/word-box.component";
import {ControlBoxComponent} from "./behavior-subject/control-box/control-box.component";
import {Form3Component} from "./parent-child/forms/form3/form3.component";
import {Form1Component} from "./parent-child/forms/form1/form1.component";
import {Form2Component} from "./parent-child/forms/form2/form2.component";
import {Form4Component} from "./parent-child/forms/form4/form4.component";
import {ParentChildHome} from "./parent-child/parent-child-home.component";
import {MenuBComponent} from './behavior-subject/menu-b/menu-b.component';
import {PanelAComponent} from './behavior-subject/panels/panel-a/panel-a.component';
import {PanelBComponent} from './behavior-subject/panels/panel-b/panel-b.component';
import {PanelCComponent} from './behavior-subject/panels/panel-c/panel-c.component';
import {CanvasPlaygroundHomeComponent} from './canvas-playground-home/canvas-playground-home.component';
import {CanvasDrawComponent} from './canvas-playground-home/canvas-draw/canvas-draw.component';
import {CanvasDrawImageComponent} from './canvas-playground-home/canvas-draw-image/canvas-draw-image.component';
import {CanvasImageManipulationComponent} from './canvas-playground-home/canvas-image-manipulation/canvas-image-manipulation.component';
import {CanvasImageManipulation2Component} from './canvas-playground-home/canvas-image-manipulation2/canvas-image-manipulation2.component';
import {DrawZonePlaygroundHomeComponent} from './draw-zone-playground/draw-zone-playground-home.component';
import {ImageZoneComponent} from './draw-zone-playground/image-zone/image-zone.component';
import {NgxCropperjsModule} from 'ngx-cropperjs';

@NgModule({
    imports: [
        BrowserModule,
        HttpClientModule,
        ReactiveFormsModule,
        NoopAnimationsModule,
        NgxCropperjsModule,
        FormsModule,
        RouterModule.forRoot([
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'parent-child'
            },
            {path: 'parent-child', component: ParentChildHome},
            {path: 'behavior-subject', component: BehaviorSubjectHomeComponent},
            {path: 'canvas-playground', component: CanvasPlaygroundHomeComponent},
            {path: 'draw-zone-playground', component: DrawZonePlaygroundHomeComponent}

        ])
    ],
    declarations: [
        AppComponent,
        TopBarComponent,
        BehaviorSubjectHomeComponent,
        ParentChildHome,
        MenuComponent,
        Form1Component,
        Form2Component,
        Form3Component,
        Form4Component,
        ControlBoxComponent,
        WordBoxComponent,
        BehaviorSubjectHomeComponent,
        MenuBComponent,
        PanelAComponent,
        PanelBComponent,
        PanelCComponent,
        CanvasPlaygroundHomeComponent,
        CanvasDrawComponent,
        CanvasDrawImageComponent,
        CanvasImageManipulationComponent,
        CanvasImageManipulation2Component,
        DrawZonePlaygroundHomeComponent,
        ImageZoneComponent,

    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
