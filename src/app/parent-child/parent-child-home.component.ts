import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './parent-child-home.component.html',
  styleUrls: ['./parent-child-home.component.css']
})
export class ParentChildHome implements OnInit {
  public currentForm : number = 0;

  constructor() { }

  handleFormChange(event: number) : void {
    this.currentForm = event;
  }
  
  
  reset(): void {
    this.currentForm = 0;
  }

  ngOnInit() {
  }

}
