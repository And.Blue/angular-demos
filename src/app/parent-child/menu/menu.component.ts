import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  @Output() selectedForm = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  toggleForm (formNb: number) : void {
    this.selectedForm.emit(formNb);
  }

}
