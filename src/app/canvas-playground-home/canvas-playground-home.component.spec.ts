import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CanvasPlaygroundHomeComponent } from './canvas-playground-home.component';

describe('CanvasPlaygroundHomeComponent', () => {
  let component: CanvasPlaygroundHomeComponent;
  let fixture: ComponentFixture<CanvasPlaygroundHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CanvasPlaygroundHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CanvasPlaygroundHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
