import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {fromEvent, Observable} from "rxjs";
import {ColorRangeRGB} from "../colorTypes";



const initialColorRange: ColorRangeRGB = {
    redMin: 20,
    redMax: 200,
    greenMin: 20,
    greenMax: 200,
    blueMin: 20,
    blueMax: 200
};


@Component({
    selector: 'app-canvas-image-manipulation',
    templateUrl: './canvas-image-manipulation.component.html',
    styleUrls: ['./canvas-image-manipulation.component.css']
})
export class CanvasImageManipulationComponent implements OnInit {
    @ViewChild('myCanvas') canvas: ElementRef<HTMLCanvasElement>;
    @ViewChild('originalImage') originalImage: ElementRef<HTMLCanvasElement>;
    public canvasOptions = {
        height: 700,
        width: 600
    };

    private image: HTMLImageElement;
    public imgLink: string = '../../../assets/link.png';
    private ctx: CanvasRenderingContext2D; //image upon which the manipulations are done
    private ctx2: CanvasRenderingContext2D; //original image

    public currentPixel: string;
    public inverted: boolean = false;

    public colorSelectors: ColorRangeRGB = initialColorRange;

    //regarding Mouse Events
    public onMove: boolean = false;
    private _mouseDown$: Observable<MouseEvent>;
    private _mouseMove$: Observable<MouseEvent>;
    private _mouseUp$: Observable<MouseEvent>;
    private _mouseIsDown: boolean;

    ngOnInit(): void {
        this.ctx = this.canvas.nativeElement.getContext('2d');
        this.ctx2 = this.originalImage.nativeElement.getContext('2d');
        this._mouseDown$ = fromEvent<MouseEvent>(this.originalImage.nativeElement, 'mousedown');
        this._mouseUp$ = fromEvent<MouseEvent>(this.originalImage.nativeElement, 'mouseup');
        this._mouseMove$ = fromEvent<MouseEvent>(this.originalImage.nativeElement, 'mousemove');
        //sets current image from Image CDN
        this.initImage(this.imgLink);
        this._mouseDown$.subscribe(
            (event: MouseEvent) => {
                this._mouseIsDown = true;
                this.retrievePixelUnderMouse(event);
            }
        );
        this._mouseUp$.subscribe(
            (event: MouseEvent) => {
                this._mouseIsDown = false;
            }
        );

        this._mouseMove$.subscribe(
            (event: MouseEvent) => {
                if (this.onMove && this._mouseIsDown) {
                }
            }
        );
    }

    //when ngModel is inverted
    public handleInverted(): void {
        this.inverted = !this.inverted;
        this.colorRangeOnly();
    }

    //gets mouse coordinates within canvas
    private getCoords(event: MouseEvent) {
        const rect = this.originalImage.nativeElement.getBoundingClientRect();
        const x = event.clientX - rect.left;
        const y = event.clientY - rect.top;
        return {x, y};
    }

    /*
        UI INTERACTIONS
     */
    removeFromCanvas(): void {
        this.ctx.clearRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
    }

    public colorRangeOnly(): void {
        this.ctx.drawImage(this.image, 0, 0);
        const imageData: ImageData = this.ctx.getImageData(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
        let data = imageData.data;

        const pixelValueFull: number = this.inverted ? 255 : 0;  //value  of pixel if it fulfills the color range selection.  
        const pixelValueEmpty: number = this.inverted ? 0 : 255;  //value  of pixel if it does not fulfills the color range selection.  

        for (let i = 0; i < data.length; i += 4) {
            if (this.colorSelectors.redMin <= data[i] && data[i] <= this.colorSelectors.redMax &&
                this.colorSelectors.greenMin <= data[i + 1] && data[i + 1] <= this.colorSelectors.greenMax &&
                this.colorSelectors.blueMin <= data[i] + 2 && data[i + 2] <= this.colorSelectors.blueMax
            ) {
                data[i] = pixelValueFull;
                data[i + 1] = pixelValueFull;
                data[i + 2] = pixelValueFull;
                data[i + 3] = 255;
            } else {
                data[i] = pixelValueEmpty;
                data[i + 1] = pixelValueEmpty;
                data[i + 2] = pixelValueEmpty;
                data[i + 3] = 255;
            }
        }
        this.ctx.putImageData(imageData, 0, 0);
    }

    //when reset button is changed
    public reset(): void {
        this.colorSelectors = {
            redMin: 20,
            redMax: 200,
            greenMin: 20,
            greenMax: 200,
            blueMin: 20,
            blueMax: 200
        };
        console.log(this.colorSelectors);
        this.colorRangeOnly();
    }

    //When trackbars change
    public onBarChange(): void {
        this.colorRangeOnly();
    }

    //changes canvas image to the one inputed.
    public uploadImage(event): void {
        this.initImage(URL.createObjectURL(event.target.files[0]));
    };

    /*
        CANVAS OPERATIONS 
     */

    //inits both original and processed images
    private initImage(link: string) {
        this.image = new Image();
        this.image.src = link;
        this.image.onload = () => {
            //draws image (the one manipulated)
            this.ctx.canvas.width = this.image.width;
            this.ctx.canvas.height = this.image.height;
            //draws image (the original one)
            this.ctx2.canvas.width = this.image.width;
            this.ctx2.canvas.height = this.image.height;
            //draws image (the one manipulated
            this.ctx.drawImage(this.image, 0, 0);
            this.ctx2.drawImage(this.image, 0, 0);
            this.colorRangeOnly();
        };
    }

    //views and returns pixel value of pointer
    public retrievePixelUnderMouse(event: MouseEvent): void {
        const {x, y} = this.getCoords(event);
        const pixelData = this.ctx2.getImageData(x, y, 1, 1).data;
        this.currentPixel = 'rgba(' + pixelData[0] + ', ' + pixelData[1] +
            ', ' + pixelData[2] + ','+pixelData[3] / 255+')';
    }

}
