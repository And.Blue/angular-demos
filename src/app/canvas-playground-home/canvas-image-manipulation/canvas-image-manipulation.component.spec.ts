import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CanvasImageManipulationComponent } from './canvas-image-manipulation.component';

describe('CanvasImageManipulationComponent', () => {
  let component: CanvasImageManipulationComponent;
  let fixture: ComponentFixture<CanvasImageManipulationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CanvasImageManipulationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CanvasImageManipulationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
