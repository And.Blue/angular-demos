import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CanvasImageManipulation2Component } from './canvas-image-manipulation2.component';

describe('CanvasImageManipulation2Component', () => {
  let component: CanvasImageManipulation2Component;
  let fixture: ComponentFixture<CanvasImageManipulation2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CanvasImageManipulation2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CanvasImageManipulation2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
