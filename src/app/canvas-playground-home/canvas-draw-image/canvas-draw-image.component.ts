import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {fromEvent, Observable} from "rxjs";


@Component({
  selector: 'app-canvas-draw-image',
  templateUrl: './canvas-draw-image.component.html',
  styleUrls: ['./canvas-draw-image.component.css']
})
export class CanvasDrawImageComponent implements OnInit {
  @ViewChild('myCanvas') canvas: ElementRef<HTMLCanvasElement>;
  public canvasOptions = {
    height: 700,
    width: 600
  };
  
  public imgLink : string = 'https://mdn.mozillademos.org/files/5395/backdrop.png';
  public onMove: boolean = false;
  private ctx: CanvasRenderingContext2D;
  private _mouseDown$: Observable<MouseEvent>;
  private _mouseMove$: Observable<MouseEvent>;
  private _mouseUp$: Observable<MouseEvent>;

  private _mouseIsDown: boolean;

  constructor() {
  }


  ngOnInit(): void {
    this.ctx = this.canvas.nativeElement.getContext('2d');
    this._mouseDown$ = fromEvent<MouseEvent>(this.canvas.nativeElement, 'mousedown');
    this._mouseUp$ = fromEvent<MouseEvent>(this.canvas.nativeElement, 'mouseup');
    this._mouseMove$ = fromEvent<MouseEvent>(this.canvas.nativeElement, 'mousemove');
    this._mouseDown$.subscribe(
        (event: MouseEvent) => {
          this._mouseIsDown = true;
        }
    );
    this._mouseUp$.subscribe(
        (event: MouseEvent) => {
          if(!this.onMove) this.draw(event);
          this._mouseIsDown = false;
        }
    );

    this._mouseMove$.subscribe(
        (event: MouseEvent) => {
          if (this.onMove && this._mouseIsDown) {
            this.draw(event);
          }
        }
    );
  }
  
  private draw(event: MouseEvent) {
    //sets positions relative to canvass
    const rect = this.canvas.nativeElement.getBoundingClientRect();
    const x = event.clientX - rect.left;
    const y = event.clientY - rect.top;
    //selects current color if selected
    const img = new Image();
    img.onload = () => {
      this.ctx.drawImage(img, x,y);
    };

    img.src = this.imgLink;
    
  }

  removeFromCanvas(): void {
    this.ctx.clearRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
  }
  

}
