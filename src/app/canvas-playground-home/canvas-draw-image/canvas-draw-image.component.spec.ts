import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CanvasDrawImageComponent } from './canvas-draw-image.component';

describe('CanvasDrawImageComponent', () => {
  let component: CanvasDrawImageComponent;
  let fixture: ComponentFixture<CanvasDrawImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CanvasDrawImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CanvasDrawImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
