
export interface MultiFormatColor {
    hex: string;
    rgb: string;
    hsl: string;
}

export interface PaletteElement {
    name: string;
    color: MultiFormatColor;
}

export interface ColorPalette extends Array<PaletteElement>{}

export const colorPalette : ColorPalette = [
    {
        name: 'red',
        color: {
            hex: '#FE0101',
            rgb: 'rgb(254, 1, 1)',
            hsl: 'hsl(0, 99%, 50%)'
        }
    },
    {
        name: 'blue',
        color: {
            hex: '#1201FE',
            rgb: 'rgb(18, 1, 254)',
            hsl: 'hsl(244, 99%, 50%)'
        }
    },
    {
        name: 'yellow',
        color: {
            hex: '#F6FE01',
            rgb: 'rgb(246, 254, 1)',
            hsl: 'hsl(62, 99%, 50%)'
        }
    },
    {
        name: 'green',
        color: {
            hex: '#28C401',
            rgb: 'rgb(40, 196, 1)',
            hsl: 'hsl(108, 99%, 39%)'
        }
    },
    {
        name: 'black',
        color: {
            hex: '#000100',
            rgb: 'rgb(0, 1, 0)',
            hsl: 'hsl(120, 100%, 0%)'
        }
    },
    {
        name: 'white',
        color: {
            hex: '#FEFEFE',
            rgb: 'rgb(254, 254, 254)',
            hsl: 'hsl(0, 0%, 100%)'
        }
    }
];
;