import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {colorPalette, ColorPalette, PaletteElement} from "./colors";
import {generateIntWithMax} from "../../utils";
import {fromEvent, Observable} from "rxjs";

@Component({
    selector: 'app-canvas-draw',
    templateUrl: './canvas-draw.component.html',
    styleUrls: ['./canvas-draw.component.css']
})
export class CanvasDrawComponent implements OnInit, OnDestroy {
    @ViewChild('myCanvas') canvas: ElementRef<HTMLCanvasElement>;
    public colors: ColorPalette;
    public selectedColor: PaletteElement;
    public canvasOptions = {
        height: 700,
        width: 600
    };
    public fillCircle: boolean = true;
    public brushSize: number = 10;
    private ctx: CanvasRenderingContext2D;
    private _mouseDown$: Observable<MouseEvent>;
    private _mouseMove$: Observable<MouseEvent>;
    private _mouseUp$: Observable<MouseEvent>;


    private _mouseIsDown: boolean;

    constructor() {
    }


    ngOnInit(): void {
        this.colors = colorPalette;
        this.ctx = this.canvas.nativeElement.getContext('2d');
        this._mouseDown$ = fromEvent<MouseEvent>(this.canvas.nativeElement, 'mousedown');
        this._mouseUp$ = fromEvent<MouseEvent>(this.canvas.nativeElement, 'mouseup');
        this._mouseMove$ = fromEvent<MouseEvent>(this.canvas.nativeElement, 'mousemove');
        this._mouseDown$.subscribe(
            (event: MouseEvent) => {
                this._mouseIsDown = true;
            }
        );
        this._mouseUp$.subscribe(
            (event: MouseEvent) => {
                this._mouseIsDown = false;
            }
        );

        this._mouseMove$.subscribe(
            (event: MouseEvent) => {
                if (this._mouseIsDown) {
                    this.draw(event);
                }
            }
        );
    }

    ngOnDestroy() {
    }

    private draw(event: MouseEvent) {
        //sets positions relative to canvass
        const rect = this.canvas.nativeElement.getBoundingClientRect();
        const x = event.clientX - rect.left;
        const y = event.clientY - rect.top;
        //selects current color if selected
        if (this.selectedColor) {
            this.ctx.strokeStyle = this.selectedColor.color.rgb;
        }
        //draws a circle depending on the brush size.
        this.ctx.beginPath();
        this.ctx.arc(x, y, this.brushSize, 0, 2 * Math.PI, false);
        if (this.fillCircle) {
            if(this.selectedColor) {
                this.ctx.fillStyle = this.selectedColor.color.rgb;
            }
            this.ctx.fill();
        }
        this.ctx.stroke();

    }

    onSelect(color: PaletteElement): void {
        this.selectedColor = color;
    }

    addRandomRectToCanvas(): void {
        const {width, height} = this.canvas.nativeElement;
        this.ctx.fillStyle = `rgb(${generateIntWithMax(255)}, 0, 0)`;
        this.ctx.fillRect(generateIntWithMax(width), generateIntWithMax(height), generateIntWithMax(100), generateIntWithMax(50));
        this.ctx.fillStyle = `rgba(0, 0, ${generateIntWithMax(255)}, 0.5)`;
        this.ctx.fillRect(generateIntWithMax(width), generateIntWithMax(height), generateIntWithMax(100), generateIntWithMax(50));

    }

    removeFromCanvas(): void {
        this.ctx.clearRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
    }

}
