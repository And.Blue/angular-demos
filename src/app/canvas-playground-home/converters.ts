import {RgbPixel, HsvPixel} from "./colorTypes";

export const hsvToRgb = (hue: number, saturation: number, value: number): RgbPixel => {
    let red, green, blue, i, f, p, q, t;

    i = Math.floor(hue * 6);
    f = hue * 6 - i;
    p = value * (1 - saturation);
    q = value * (1 - f * saturation);
    t = value * (1 - (1 - f) * saturation);
    switch (i % 6) {
        case 0:
            red = value, green = t, blue = p;
            break;
        case 1:
            red = q, green = value, blue = p;
            break;
        case 2:
            red = p, green = value, blue = t;
            break;
        case 3:
            red = p, green = q, blue = value;
            break;
        case 4:
            red = t, green = p, blue = value;
            break;
        case 5:
            red = value, green = p, blue = q;
            break;
    }
    return {
        red: Math.round(red * 255),
        green: Math.round(green * 255),
        blue: Math.round(blue * 255)
    };
};


//converted formula from https://www.rapidtables.com/convert/color/rgb-to-hsv.html
export const rgbToHsv = (red: number, green: number, blue: number): HsvPixel => {
    let max = Math.max(red, green, blue), min = Math.min(red, green, blue),
        d = max - min,
        h,
        s = (max === 0 ? 0 : d / max),
        v = max / 255;

    switch (max) {
        case min:
            h = 0;
            break;
        case red:
            h = (green - blue) + d * (green < blue ? 6 : 0);
            h /= 6 * d;
            break;
        case green:
            h = (blue - red) + d * 2;
            h /= 6 * d;
            break;
        case blue:
            h = (red - green) + d * 4;
            h /= 6 * d;
            break;
    }
    return {
        hue: Math.round(h * 360), //for degrees
        saturation: roundedToPercent(s),
        value: roundedToPercent(v)
    };
};

export const roundedToPercent = (value: number): number => Math.round(value * 1000) / 10;

export const radiansToDegrees = (value: number): number => {
    return Math.round(value * 180 / Math.PI);
};


