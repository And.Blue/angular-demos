export interface ColorRangeHSV {
    valueMax: number;
    valueMin: number;
    saturationMax: number;
    saturationMin: number;
    hueMax: number;
    hueMin: number;
}

export interface ColorRangeRGB {
    redMax: number;
    redMin: number;
    greenMax: number;
    greenMin: number;
    blueMax: number;
    blueMin: number;
}


export interface RgbPixel {
    red: number;
    green: number;
    blue: number;
}

export interface HsvPixel {
    hue: number;
    saturation: number;
    value: number;
}
