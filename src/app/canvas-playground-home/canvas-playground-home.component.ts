import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-canvas-playground-home',
  templateUrl: './canvas-playground-home.component.html',
  styleUrls: ['./canvas-playground-home.component.css']
})
export class CanvasPlaygroundHomeComponent implements OnInit{
  public panel: string = 'colorHsv';
  
  ngOnInit(): void {
  }

}
