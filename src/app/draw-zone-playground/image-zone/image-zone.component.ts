import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import Cropper from 'cropperjs';
import {NgxCropperjsComponent} from "ngx-cropperjs";

interface boxMarker {
    xMin: number;
    xMax: number;
    yMin: number;
    yMax: number;
}

@Component({
    selector: 'app-image-zone',
    templateUrl: './image-zone.component.html',
    styleUrls: ['./image-zone.component.css']
})
export class ImageZoneComponent implements OnInit {
    @ViewChild('imageToAnnotate') cropperComponent: NgxCropperjsComponent;
    @ViewChild('markerActivator') markerActivator: ElementRef;
    @ViewChild('markerControls') markerControls: ElementRef;
    @ViewChild('imageResult') imageResult: ElementRef;
    @ViewChild('download') download: ElementRef;

    private image: HTMLImageElement;
    public imgLink: string = '../../../assets/bg2/IMG_3504_small.jpg';
    public cropperConfig: any;
    private setting = {
        element: {
            dynamicDownload: null as HTMLElement
        }
    };


    public currentPixel: string;
    public markers: boxMarker[] = [];


    ngOnInit(): void {
        this.initImage(this.imgLink);
        this.cropperConfig = {
            center: true,
            guides: false,
            viewMode: 3,
            modal: false,
            aspectRatio: NaN,
            cropboxResizable: true,
            zoomable: false,
            autoCrop: true,
        };
    }

    public getAnnotationInfo() {

    }

    //changes canvas image to the one inputed.
    public uploadImage(event): void {
        this.initImage(URL.createObjectURL(event.target.files[0]));
    };


    public saveBoundingBox(): void {
        const {width, height, left, top} = this.cropperComponent.cropper.cropBoxData;

        this.markers.push({
            xMin: Math.round(left),
            xMax: Math.round(left + width),
            yMin: Math.round(top),
            yMax: Math.round(top + height)
        })

    }


    private initMarker(): void {
        this.markerActivator.nativeElement.style.display = 'none';
        this.markerControls.nativeElement.style.display = '';
    }

    public deleteMarker(marker: boxMarker) {
        this.markers = this.markers.filter(value => value !== marker);
    }


    public downloadBoxJson(): void {
        const splitSrc = this.image.src.split('/');

        this.dyanmicDownloadByHtmlTag({
                fileName: `${splitSrc[splitSrc.length - 1].split('.')[0]}.json`,
                text: JSON.stringify(this.markers)
            }
        );

    }

    //inits both original and processed images
    private initImage(link: string): void {
        this.image = new Image();
        this.image.src = link;
    }


    private dyanmicDownloadByHtmlTag(arg: { fileName: string, text: string }) {
        if (!this.setting.element.dynamicDownload) {
            this.setting.element.dynamicDownload = document.createElement('a');
        }

        const element = this.setting.element.dynamicDownload;
        const fileType = arg.fileName.indexOf('.json') > -1 ? 'text/json' : 'text/plain';
        element.setAttribute('href', `data:${fileType};charset=utf-8,${encodeURIComponent(arg.text)}`);
        element.setAttribute('download', arg.fileName);

        var event = new MouseEvent("click");
        element.dispatchEvent(event);
    }

}
