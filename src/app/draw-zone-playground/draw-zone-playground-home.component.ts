import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-draw-zone-playground',
  templateUrl: './draw-zone-playground-home.component.html',
  styleUrls: ['./draw-zone-playground-home.component.css']
})
export class DrawZonePlaygroundHomeComponent implements OnInit {
  public panel: string = 'select-box';
  constructor() { }

  ngOnInit() {
  }

}
