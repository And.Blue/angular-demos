import { Component, OnInit } from '@angular/core';
import { PanelStoreService} from "../../panel-store.service";
import {generateIntWithMax} from "../../../utils";

@Component({
  selector: 'app-panel-b',
  templateUrl: './panel-b.component.html',
  styleUrls: ['./panel-b.component.css']
})
export class PanelBComponent implements OnInit {
  public panelContent: number[]  = [];

  constructor(private panelStore: PanelStoreService) { }

  ngOnInit() {
    this.panelStore.panelState$.subscribe(
        panelState => this.panelContent = panelState.panelB
    );

  }
}
