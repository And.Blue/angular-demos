import {Component, OnInit} from '@angular/core';
import {PanelStoreService} from "../../panel-store.service";
import {generateIntWithMax} from "../../../utils";

@Component({
    selector: 'app-panel-c',
    templateUrl: './panel-c.component.html',
    styleUrls: ['./panel-c.component.css']
})
export class PanelCComponent implements OnInit {
    public panelContent: number[] = [];

    constructor(private panelStore: PanelStoreService) {
    }

    ngOnInit() {
        this.panelStore.panelState$.subscribe(
            panelState => this.panelContent = panelState.panelC
        );

    }
}
