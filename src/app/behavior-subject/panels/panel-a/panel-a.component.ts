import {Component, OnInit} from '@angular/core';
import {PanelStoreService} from "../../panel-store.service";

@Component({
    selector: 'app-panel-a',
    templateUrl: './panel-a.component.html',
    styleUrls: ['./panel-a.component.css']
})
export class PanelAComponent implements OnInit {
    public panelContent: number[] = [];

    constructor(private panelStore: PanelStoreService) {
    }

    ngOnInit() {
        this.panelStore.panelState$.subscribe(
            panelState => this.panelContent = panelState.panelA
        );

    }

}
