import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {PanelStoreService} from "../panel-store.service";

@Component({
  selector: 'app-menu-b',
  templateUrl: './menu-b.component.html',
  styleUrls: ['./menu-b.component.css']
})
export class MenuBComponent implements OnInit {
  @Output() selectedForm = new EventEmitter<number>();

  constructor(private panelStore: PanelStoreService) { }

  ngOnInit() {
  }

  toggleForm (formNb: number) : void {
    this.selectedForm.emit(formNb);
  }
  
  addToPanel(panelId: number) {
    this.panelStore.addValueToPanel(panelId);
  }
  
  removeFromPanel(panelId: number) {
    this.panelStore.removeFromPanel(panelId)
    
  }
  
  
  
  
  
  
}
