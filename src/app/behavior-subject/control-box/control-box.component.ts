import { Component, OnInit } from '@angular/core';
import {WordService} from "../word-box/word.service";

@Component({
  selector: 'app-control-box',
  templateUrl: './control-box.component.html',
  styleUrls: ['./control-box.component.css']
})
export class ControlBoxComponent implements OnInit {

  constructor(private wordService: WordService) { }

  ngOnInit() {
  }

  send(value: string): void {
    this.wordService.addWord(value);
  }

}
