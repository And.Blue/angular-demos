import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {generateIntWithMax} from "../utils";


//Contains the State of The 3 panels
export interface AllPanelValues {
    panelA: number[],
    panelB: number[],
    panelC: number[]
}

const initialState = {
    panelA: [],
    panelB: [],
    panelC: []
} as AllPanelValues;

@Injectable({
    providedIn: 'root'
})
export class PanelStoreService {
    private _panelStateSubject$ = new BehaviorSubject<AllPanelValues>(initialState);
    
    //Notifies all Panel Components that their values changed
    public panelState$ = this._panelStateSubject$.asObservable();

    constructor() {
    }

    /**
     * Finds panel Id and adds a random Int to the subscribing Panel
     */
    addValueToPanel(panelId: number): void {
        let currentState: AllPanelValues = this._panelStateSubject$.getValue();
        switch (panelId) {
            case 1:
                currentState.panelA.push(generateIntWithMax(666));
                break;
            case 2:
                currentState.panelB.push(generateIntWithMax(111));
                break;
            case 3:
                currentState.panelC.push(generateIntWithMax(333));
                break;
            default:
                throw Error("Something went wrong while adding to panel");
        }
        this._panelStateSubject$.next(currentState);
    }

    /**
     * Finds panel Id and removes a random Int to the subscribing Panel
     */
    removeFromPanel(panelId: number): void {
        let currentState: AllPanelValues = this._panelStateSubject$.getValue();
        switch (panelId) {
            case 1:
                currentState.panelA.pop();
                break;
            case 2:
                currentState.panelB.pop();
                break;
            case 3:
                currentState.panelC.pop();
                break;
            default:
                throw Error("Something went wrong while removing from panels");
        }
        this._panelStateSubject$.next(currentState)
    }
}
