
export const SampleWords = [{value: "dogs", id:0}, {value: "cats", id:1}];

export interface Word {
    value: string;
    id: number;
}
