import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-behavior-subject-home',
  templateUrl: './behavior-subject-home.component.html',
  styleUrls: ['./behavior-subject-home.component.css']
})
export class BehaviorSubjectHomeComponent implements OnInit {
  public currentPanel : number = 0;

  constructor() { }

  handleFormChange(event: number) : void {
    this.currentPanel = event;
  }


  reset(): void {
    this.currentPanel = 0;
  }

  ngOnInit() {
  }
}
