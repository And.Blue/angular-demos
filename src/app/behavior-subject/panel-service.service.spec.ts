import { TestBed } from '@angular/core/testing';

import { PanelStoreService } from './panel-store.service';

describe('PanelStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PanelStoreService = TestBed.get(PanelStoreService);
    expect(service).toBeTruthy();
  });
});
