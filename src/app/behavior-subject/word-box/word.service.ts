import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {SampleWords, Word} from "../sampleWords";

@Injectable({
    providedIn: 'root'
})
export class WordService {
    private wordSubject = new BehaviorSubject<Word[]>([] as Word[]);
    public words = this.wordSubject.asObservable();

    constructor() {
    }

    addWord(word: string): void {
        this.wordSubject.next([...this.wordSubject.getValue(), {value: word, id: Math.random()}])
    }  
    
    removeWord(word: string): void {
        this.wordSubject.next(this.wordSubject.getValue().filter(arg => arg.value !== word));
    } 

    loadSamples(): void {
        this.wordSubject.next(SampleWords);
    }
}
