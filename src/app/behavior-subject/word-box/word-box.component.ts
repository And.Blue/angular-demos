import {Component, OnInit} from '@angular/core';
import {Word} from "../sampleWords";
import {WordService} from "./word.service";


@Component({
    selector: 'app-word-box',
    templateUrl: './word-box.component.html',
    styleUrls: ['./word-box.component.css']
})
export class WordBoxComponent implements OnInit {

    public words: Word[];

    constructor(private wordService: WordService) {
    }

    ngOnInit() {
        this.wordService.words.subscribe(
            words => this.words = words,
            err => console.log(err)
        );

    }

    deleteWord(word: string) {
        this.wordService.removeWord(word);        
    }

}
