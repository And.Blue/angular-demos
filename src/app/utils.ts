
export const generateIntWithMax = (max: number): number => {
    return Math.floor(Math.random() * max) + 1
};